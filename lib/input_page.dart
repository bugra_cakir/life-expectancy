import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:life_expectancy/constants.dart';
import 'package:life_expectancy/user_data.dart';

import 'MyColumn.dart';
import 'MyContainer.dart';
import 'result_page.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  int _boy = 160;
  int _kilo = 60;
  double _icilenSigara = 0;
  double _haftalikSpor = 0;
  Color textColor = Colors.black;
  late String cinsiyet;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'YAŞAM BEKLENTİSİ',
          style: TextStyle(color: Colors.black54),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: MyContainer(
                    child: buildRowOutlineButton('BOY', _boy),
                  ),
                ),
                Expanded(
                  child: MyContainer(
                    child: buildRowOutlineButton('KİLO', _kilo),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: MyContainer(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Haftada Yapılan Spor(Gün)',
                    textAlign: TextAlign.center,
                    style: kMetinStili,
                  ),
                  Text(_haftalikSpor.round().toString(),
                      textAlign: TextAlign.center, style: kSayiStili),
                  Slider(
                    activeColor: Colors.lightGreen,
                    inactiveColor: Colors.black54,
                    min: 0,
                    max: 7,
                    value: _haftalikSpor,
                    label: '$_haftalikSpor',
                    onChanged: (value) {
                      setState(() {
                        _haftalikSpor = value;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: MyContainer(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Günlük içilen sigara',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                  Text(
                    _icilenSigara.round().toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: (_icilenSigara >= 20)
                            ? Colors.red
                            : (_icilenSigara < 20 && _icilenSigara > 10)
                                ? Colors.orange
                                : Colors.black54,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                  Slider(
                    activeColor: Colors.red,
                    inactiveColor: Colors.black54,
                    min: 0,
                    max: 40,
                    value: _icilenSigara,
                    onChanged: (value) {
                      setState(() {
                        _icilenSigara = value;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        cinsiyet = 'KADIN';
                        if (cinsiyet == 'KADIN') {
                          color:
                          Colors.blueGrey;
                        }
                      });

                      print('Cinsiyet Seçildi ');
                    },
                    child: MyContainer(
                      color:
                          cinsiyet == 'KADIN' ? Colors.blueGrey : Colors.white,
                      child: MyColumn(
                        iconcuk: FontAwesomeIcons.female,
                        logoName: 'KADIN',
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        cinsiyet = 'ERKEK';
                        if (cinsiyet == 'ERKEK') {
                          color:
                          Colors.blueGrey;
                        }
                      });
                    },
                    child: MyContainer(
                      color:
                          cinsiyet == 'ERKEK' ? Colors.blueGrey : Colors.white,
                      child: MyColumn(
                        iconcuk: FontAwesomeIcons.male,
                        logoName: 'ERKEK',
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          ButtonTheme(
            height: 50,
            child: FlatButton(
              color: Colors.white,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ResultPage(UserData(
                        kilo: _kilo,
                        cinsiyet: cinsiyet,
                        boy: _boy,
                        haftalikSpor: _haftalikSpor,
                        icilenSigara: _icilenSigara)),
                  ),
                );
              },
              child: Text(
                'HESAPLA',
                style: kMetinStili,
              ),
            ),
          )
        ],
      ),
    );
  }

  Row buildRowOutlineButton(String label, int valuuWantToChange) {
    return Row(
      children: <Widget>[
        SizedBox(width: 15),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            OutlinedButton(
              onPressed: () {
                setState(() {
                  label == 'BOY' ? _boy++ : _kilo++;
                  print('üst basıldı');
                });
              },
              onLongPress: () {
                setState(() {
                  label == 'BOY' ? _boy = _boy + 10 : _kilo = _kilo + 10;
                  print('uzun arttırma basıldı');
                });
              },
              child: const Icon(Icons.add_circle),
            ),
            OutlinedButton(
              onPressed: () {
                setState(() {
                  label == 'BOY' ? _boy-- : _kilo--;

                  print('$valuuWantToChange');
                });
              },
              onLongPress: () {
                setState(() {
                  label == 'BOY' ? _boy = _boy - 10 : _kilo = _kilo - 10;

                  print('uzun azaltma basıldı');
                });
              },
              child: const Icon(Icons.remove_circle),
            ),
          ],
        ),
        SizedBox(width: 15),
        RotatedBox(
          quarterTurns: -1, //ÇeyrekDönüş
          child: Text(
            label,
            style: kMetinStili,
          ),
        ),
        SizedBox(width: 15),
        RotatedBox(
          quarterTurns: -1,
          child: Text(
            label == 'BOY' ? '$_boy' : '$_kilo',
            style: kMetinStili,
          ),
        ),
        SizedBox(width: 15),
      ],
    );
  }
}
