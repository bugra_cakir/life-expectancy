import 'package:flutter/material.dart';
import 'package:life_expectancy/constants.dart';
import 'package:life_expectancy/user_data.dart';

import 'Hesap.dart';

class ResultPage extends StatelessWidget {
  final UserData _userData;
  ResultPage(this._userData);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('SONUÇ SAYFASI')),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Center(
                child: Text(
              'Beklenen Yaşam Süresi',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black54,
                fontSize: 50,
              ),
            )),
          ),
          Expanded(
            flex: 8,
            child: Center(
                child: Text(
              Hesap(user_Data: _userData).hesaplama().round().toString(),
              style: TextStyle(color: Colors.white, fontSize: 60),
            )),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: FlatButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'Geri Dön',
                  style: kMetinStili,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
