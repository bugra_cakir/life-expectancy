import 'package:life_expectancy/user_data.dart';

class Hesap {
  UserData user_Data;

  Hesap({required this.user_Data});

  double hesaplama() {
    double sonuc;

    sonuc = 80 + user_Data.haftalikSpor - user_Data.icilenSigara;
    sonuc = sonuc + (user_Data.boy / user_Data.kilo);
    if (user_Data.cinsiyet == 'KADIN') {
      return sonuc + 5;
    } else {
      return sonuc;
    }
  }
}
