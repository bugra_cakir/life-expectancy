class UserData {
  int boy;
  int kilo;
  double icilenSigara;
  double haftalikSpor;
  String cinsiyet;

  UserData(
      {required this.boy,
      required this.kilo,
      required this.icilenSigara,
      required this.haftalikSpor,
      required this.cinsiyet});
}
